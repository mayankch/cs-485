﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;
    public Text looseText;
    private Rigidbody rb;
    private int count;
    private int countlife;
    public bool onGround;
    public bool onWall;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        countlife=3;
        onGround = true;
        onWall = true;
        SetCountText();
        winText.text = "";
        looseText.text = "";
  
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        if (onGround)
        {
            if (Input.GetButtonDown("Jump"))
            {
                rb.velocity = new Vector3(0f, 8f, 0f);
                onGround = false;
            }
        }

        if (onWall)
        {
            if (Input.GetButtonDown("Jump"))
            {
                rb.velocity = new Vector3(0f, 8.1f, 0f);
                onWall = false;
            }
        }

        if (countlife == 0)
        {
            looseText.text = "Game Over :( Start Again";
        }

        if (transform.position.y <= -25)
        {
            countlife = countlife - 1;
            // Application.LoadLevel(Application.loadedLevel); 

            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
        
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Ground"))
        {
            onGround = true;
        }

        if (other.gameObject.CompareTag("OnWall"))
        {
            onWall = true;
        }
    }


    void OnTriggerEnter(Collider other)
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            audio.Play();
            SetCountText();
        }
        else if (other.gameObject.CompareTag("BigPickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 5;
            audio.Play();
            SetCountText();
        }
    }

  
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString() + "/38 \nSmall PickUps = 1 point \nBig Pickups = 5 points";
        if (count >=38)
        {
            winText.text = "Congratulations! You're a winner!";
        }
    }
}